package br.com.senac.cachoeirasdoes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnriodomeio;
    private Button btnmatilde;
    private Button btnpalito;
    private Button btnveudenoiva;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnriodomeio = findViewById(R.id.btnriodomeio);
        btnmatilde = findViewById(R.id.btnmatilde);
        btnpalito = findViewById(R.id.btnpalito);
        btnveudenoiva = findViewById(R.id.btnveudenoiva);

        btnriodomeio.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RiodoMeioActivity.class);
                startActivity(intent);
            }
        });

        btnmatilde.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, MatildeActivity.class);
                startActivity(intent);
            }
        });

        btnpalito.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PalitoActivity.class);
                startActivity(intent);
            }
        });

        btnveudenoiva.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,VeudeNoivaActivity.class);
                startActivity(intent);
            }
        });



    }

}